from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
from tkinter.ttk import *


def main():
    def bt_click():
        win.filename = filedialog.askopenfilename(initialdir= "c:/qimob/remessa/", title="Selecione a Remessa", filetypes=(("jpeg files","*.rem"),("all files","*.*")))
        if win.filename:
            f = open(win.filename, "r")
            contents = f.readlines()

            # Verifica se o arquivo pertende ao cedente correto
            x = contents[0]
            if x[39:46] == '5091549':
                file = open(win.filename, "w+")

                for x in contents:
                    if len(x) > 401:
                        if x[0] == '1':
                            # Faz a correção
                            x = x[:70] + x[81:]

                            # Campo de define a porcentagem com 4 dígitos 2% = 0200
                            juros = '0200'
                            x = x[:66] + juros + x[70:]
                            print()
                    file.write(x)
                file.close()
                f.close()
                messagebox.showinfo("OK", "Remessa corrida com sucesso")
                win.quit()
            else:
                messagebox.showerror("Inválido", "Remessa selecionada não pertence ao cedente 5091549")

            f.close()


    win = Tk()
    win.title("Correção Remessa")
    win.resizable(False, False)

    style = Style()

    style.configure('W.TButton', font=
    ('calibri', 10, 'bold', 'underline'),
                    foreground='red')

    bt = Button(win, width='30', text='SELECIONAR ARQUIVO', style='W.TButton', command=bt_click)
    bt.place(relx=0.5, rely=0.5, anchor=CENTER)


    window_height = 100
    window_width = 250

    screen_width = win.winfo_screenwidth()
    screen_height = win.winfo_screenheight()

    x_cordinate = int((screen_width/2) - (window_width/2))
    y_cordinate = int((screen_height/2) - (window_height/2))

    win.geometry("{}x{}+{}+{}".format(window_width, window_height, x_cordinate, y_cordinate))

    win.mainloop()

if __name__=="__main__":
    main()
